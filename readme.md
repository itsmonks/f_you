## Simple script to make alexa swear back when given the finger
1. Install appropriate libraries
2. Create a voice monkey account
3. Add a routine you want it to execute in your alexa app
4. Fill in config.ini appropriately
5. Run middle_finger.py

# Extra credit to 
https://www.youtube.com/watch?v=p5Z_GGRCI5s

With this youtuber's video I was able to come up with the recognition for the motion of the middle finger.
