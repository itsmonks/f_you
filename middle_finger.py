import cv2
import configparser
import requests
from datetime import datetime
import handtrackingmodule as htm

def eligible_to_run(last_run):
    if last_run is None:
        return True
    
    time_diff = datetime.now() - last_run
    return time_diff.total_seconds() >= 10

def execute_speech(config):

    try:
        access_config = config.get('voicemonkey', 'access_token')
        secret_config = config.get('voicemonkey', 'secret_token')
        monkey_config = config.get('voicemonkey', 'monkey')

        base_url = "https://api.voicemonkey.io/trigger?"
        access_token = "access_token=" + access_config + "&"
        secret_token = "secret_token=" + secret_config + "&"
        monkey = "monkey=" + monkey_config
        
        request_url = base_url + access_token + secret_token + monkey

        r = requests.get(request_url)

        return r.status_code
    except ValueError:
        print("Please ensure you have your configurations set properly in config.ini")
        return False
    except Exception as e:
        print("Failed to execute request. Error: " + e)
        return False

def detect_middle_finger(landmarks):

    return (landmarks[4][1] > landmarks[3][1] and   # Thumb
            landmarks[8][2] > landmarks[6][2] and   # Index
            landmarks[12][2] < landmarks[10][2] and # Middle finger
            landmarks[16][2] > landmarks[14][2] and # Ring Finger
            landmarks[20][2] > landmarks[18][2])    # Pinky
    
def main():

    configParser = configparser.RawConfigParser()
    configFilePath = r'config.ini'
    configParser.read(configFilePath)

    last_run = None

    cap = cv2.VideoCapture(1)
    detector = htm.handDetector(detectionCon=0.75)
    while True:
        success, img = cap.read()
        img = detector.findHands(img)

        landmarks = detector.findPosition(img, draw=False)

        if landmarks and detect_middle_finger(landmarks) and eligible_to_run(last_run):
            result = execute_speech(configParser)
            last_run = datetime.now()
            if result:
                print("Executed command succesfully")

        cv2.imshow("Image", img)
        if cv2.waitKey(5) & 0xFF == ord('q'):
            break
    cap.release()

if __name__ == "__main__":
    main()